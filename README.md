# Pari package for anaconda

## Install

```
micromamba create -n pari -c pascal.molin -c conda-forge pari
```

## Build

### configure a conda environment

```
conda create -n condabuild conda-build anaconda-client conda-verify
```


### build pari/GP

```
conda activate condabuild
conda build .
```

### upload to Anaconda

```
anaconda login
anaconda upload /home/molin/miniconda3/envs/condabuild/conda-bld/linux-64/pari-2.15.2-h9bf148f_0.tar.bz2
```

### use it

```
conda install -c conda-forge pari-elldata pari-galdata
conda install -c pascal.molin pari
```
